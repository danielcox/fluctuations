\documentclass[preprint,pra,aps,amsmath,amssymb,showpacs,superscriptaddress]{revtex4-1}

\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{soul,xcolor}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage[T1]{fontenc}
\usepackage{hyperref}

%\documentclass[twocolumn]{article}
%
%\usepackage{graphicx}
\usepackage{siunitx}
\usepackage{mhchem}

\begin{document}
%Gummi|065|=)
\title{Mesoscopic Shot-Noise Fluctuations in Graphene}

\author{NANO}
\affiliation{Low Temperature Laboratory, Department of Applied Physics, Aalto University, Espoo, Finland}

%\author{Daniel Cox}
%\affiliation{Low Temperature Laboratory, Department of Applied Physics, Aalto University, Espoo, Finland}
%\author{Teemu Elo}
%\affiliation{Low Temperature Laboratory, Department of Applied Physics, Aalto University, Espoo, Finland}
%\author{Zhenbing Tan}
%\affiliation{Low Temperature Laboratory, Department of Applied Physics, Aalto University, Espoo, Finland}
%\author{N.N}
%\affiliation{Low Temperature Laboratory, Department of Applied Physics, Aalto University, Espoo, Finland}
%\author{Pertti J. Hakonen}\email[Corresponding author: pertti.hakonen@aalto.fi]{}
%\affiliation{Low Temperature Laboratory, Department of Applied Physics, Aalto University, Espoo, Finland}

\begin{abstract}
We report the first measurements of mesoscopic shot-noise power fluctuations in graphene as a function of gate voltage (Fermi energy) and bias at low temperature in zero-field and a weak perpendicular magnetic field. We find the behaviour of shot-noise fluctuations to be coherent with that of conductance fluctuations but note some deviations from theory concerning diffusive metal systems.\textbf{**(FIX merge this version with other texts on notes - add references)**}
\end{abstract}

\pacs{}

\maketitle

\section{Introduction}
Universal conductance fluctuations (UCF) arise from the interference of phase-coherent electrons in diffusive, mesoscopic systems; the average amplitude of these conductance fluctuations is on the order of 1 $e^2/h$, independent of the mean conductance.

Electrical conduction in graphene ribbons is strongly influenced by disorder which brings about competition between diffusive transport and and hopping conduction caused by  localization of charge carriers. Localization and hopping conduction are dominant near the charge neutrality point, while away for the Dirac point,  graphene system behaves very similarly to a diffusive conductor. Uniquely, the competition between inter- and intravalley scattering leads to interplay of weak localization and antilocalization phenomena in graphene.

In the diffusive regime, the charge density can be varied by gating in graphene. This significantly modifies screening of impurities and  leads to effectively reconfigured scattering scattering configuration of charged impurities.  As a consequence, many of the quantum corrections in electrical transpoort are visible in gate sweeps on graphene samples. These phenomena include universal conductance fluctuations and, as we demonstrate here, universal shot noise fluctuations.

The first studies of GNRs down to width $W \simeq  20$ nm \cite{chen2007,Han2007} demonstrated the presence of a transport gap inversely proportional to the width and independent on the crystallographic orientation \cite{Han2007}.  Similar transport gaps were observed for much smaller ribbon width in GNRs fabricated using sonication of intercalated graphite in solution, indicating smoother edges than the etched GNRs \cite{li2008}. The experiments performed on GNRs \cite{chen2007,Han2007,ozyilmaz2007,Han2010,Oostinga2010, Danneau2010} have clearly indicated variable range hopping while the role of the Coulomb gap has remained elusive \cite{sols2007,Droscher2011}. Shot noise has been employed in these studies especially to distinguish between elastic and inelastic processes in GNRs \cite{Danneau2010, Tan2013} .

It has been demonstrated that, according to shot noise experiments on high-quality etched GNRs, a Coulomb gap is formed which leads to enhanced shot noise in the graphene ribbon near the Dirac point \cite{Danneau2010, Tan2013} .  Away from the Dirac point, the shot noise  is rather constant and diffusive behavior is assumed \cite{DiCarlo2008,Tan2013}. However, our results indicate that the shot noise does clearly fluctuate as a function of the gate voltage. Such fluctuations are predicted for a diffusive system by de Jong and Beenakker \cite{Jong1995},   which is also in accordance with the numerical simulations \cite{lewenkopf2008} on disordered graphene ribbons. However, the theoretical works on graphene have not calculated the amount of fluctuating shot noise power. In our work, we present first experimental results on shot noise power fluctuations in GRNs of  50 nm width and compare them with tight binding calculations. We find that the shot noise power fluctuations ($\delta P_{rms}/P_0$, see below) are relatively stronger than conductance fluctuations $\delta G_{rms}/G_0$ where $G_0=e^2/h$.  Our results are consistent with theoretical models for diffusive systems and hint towards broken time reversal symmetry in the absence of magnetic field

Our studies of shot noise fluctuations were made  at low temperatures around 50 mK where the contribution of inelastic scattering events is expected to be small and scattering matrix theory should be applicable. We employ the Fano factor $F$  \cite{blanter2000} , given by the ratio of shot noise and mean current, to quantify shot noise; the noise power spectrum is given by $S(I) = F \times 2eI$. In the scattering matrix formalism \cite{blanter2000},
$F = \sum_{n=1}^{N}T_n(1-T_n)/\sum_{n=1}^{N} T_n$,  where the individual terms depend only on the  $N^{th}$ transmission eigenvalue $T_n$. For an ideal diffusive conductor having a bimodal distribution of transmission eigenvalues, one obtains  $F=\frac{1}{3}$ while for a chaotic quantum dot $F=\frac{1}{4}$.
According to theoretical calculations on ballistic graphene, $F=\frac{1}{3}$ for large width over length ratio ($\frac{W}{L}>3$) due to Klein tunneling \cite{katsnelson2006a,Tworzydio2006}, as also indicated by the experiments \cite{Danneau2008,DiCarlo2008}. Smooth potential disorder is found to decrease $F$ \cite{sanjose2007}, but even increased levels of noise can be observed when the disorder is strong,  \cite{lewenkopf2008}.

Shot noise fluctuations are expected for an ensemble of diffusive conductors \cite{Jong1995}. Here, instead of using an ensemble, we have to be satisfied with a data set comprising of a gate sweep on a sample where two separate sections are measured (see Fig. \ref{sample}). The movement of Fermi level by gating will change screening of impurities and the scattering configuration, so the set will have more correlations and smaller variation is expected for the universal fluctuations of conductance and shot noise. This is true for the variation of conductance in our experiments but the variation of shot noise turns out to be on the same order as predicted by theory for ensembles.


\textbf{**(FIX marge other documents here and collect written notes)**}

\section{Background}

According to theory, shot noise fluctuations are determined by the relation:
\
\begin{equation}\label{shotnoiserms}
\delta P_{rms}=2 e |V| \frac{2e^2}{h} C_{\beta}= 2e |I| G \frac{2e^2}{h} C_{\beta}
\end{equation}
where the parameter $C_{\beta}$ depends on the nature of scattering. The latter form displays that the variation of the Fano factor does also involve the conductance of the system.
The theoretical result for shot noise reads:
$C_{\beta}^2 =\frac{1}{\beta} \frac{46}{2835}$.
This yields $C_{\beta}=0.127$, $C_{\beta}=0.090$, and $C_{\beta}=0.067$   for orthogonal ($\beta = 1$ ) and unitary ($\beta = 2$), and symplectic ($\beta = 4$) ensembles.

\[{\rm{rms }}P = 2e\left| V \right|\frac{{2{e^2}}}{h}C_{\beta}{\left( {\frac{{{\ell _\varphi }}}{L}} \right)^{5/2}}\]


In 2-dim graphene, $\beta = 1$ when the system has intervalley scattering, while with broken time reversal symmetry, for example due to boundary conditions \cite{Richter2012}, $\beta = 2$. The further theoretical possibility, $\beta = 4$, would mean that there is only intravalley scattering present, but this is not found in the tight binding simulations of Ref. \onlinecite{Richter2012}. Conductance fluctuations in these three scattering regimes are given by xxx fixme.
In Ref. \onlinecite{Richter2012}, edge scattering is found to be important factor in determining  $\beta$ in graphene nanoribbons, and it may lead to unitary behavior instead of orthogonal structure at zero field.

The phase coherence length in 2d with electron-electron interaction effects is given by

\[\frac{1}{{{\tau _N}}} \simeq \frac{{{k_B}T}}{\hbar }{G_Q}R \times \ln \frac{1}{{{G_Q}R}}\]

where  $G_Q=2e^/h$ is the the conductance quantum and $R_{box}$ denotes the sheet resistance. 

The coherence length is then $\ell_{\varphi} =\sqrt{D \tau_N}$. 

Nyquist time  
\section{Experimental Details}

All measured samples are single-layer natural graphene flakes deposited on \SI{300}{\nano\meter} \ce{SiO2} with a n++ doped silicon back gate using the scotch tape method. Thickness is confirmed using light contrast and Raman spectroscopy.

All samples were two-terminal with \ce{Au}/\ce{Cr}/\ce{Au} contacts deposited with an e-beam evaporator under UHV conditions.

The samples were measured in a Bluefors BD-SD250 dry dilution cryostat with a base temperature of \SI{50}{\milli\kelvin}\textbf{**(Add fridge schematic, add more details)**}

In our  analysis of $G$ fluctuations, we assume that the background conduction grows linearly with the gate voltage when going away from the Dirac point, located in the middle of the transport gap (see dashed line in Fig. \ref{Grms}).
Our results for the rms-variation of conductance (fixme variance$^{0.5}$) at $V_g < 0$ is 0.164 and 0.102 at $V_g > 0$ with linear background subtracted and 0.42 and 0.32, respectively,  with average background removed. Consequently, we estimate $\delta G_{rms}/G_0 = 0.25 \pm 0.1$ from our experiment. The obtained value for $\delta G_{rms}/G_0$ is  below the theoretical value $var(G/G_0)^{0.5} = \sqrt{4/15} \simeq 0.5$ for unitary diffusive wire (with $G_0=e^2/h$) and also slightly below $var(G/G_0)^{0.5} =0.35$  the symplectic case with $\beta =4$.  However, one has to note that there is slight uncertainty with the subtracted background as  electrostatic (Coulomb) effects may modify the linear dependence. Coulomb effects might modify the behavior towards quantum dots where smaller fluctuations than in diffusive wires  are predicted,  depending on the asymmetry of the contacts. With all likelyhood, part of the discrepancy between theory and our measurement originates from the fact that the gate sweep is not fully able to generate independent impurity configurations and the conductance fluctuations remain slightly below the theoretical ensemble results.

\section{Results \& Discussion}


The sample was DC-biased in a constant-current regime, differential resistance was measured with a lock-in amplifier at \SI{5.6}{\hertz}, the sample back-gate was connected to a high-voltage DC supply. All DC connections had RC low-pass filters at mixing chamber temperature. The cross-correlation spectrometer samples mixed-down RF at a centre frequency of \SI{650}{\mega\hertz} with a sampling rate of \SI{180}{\mega S\per\second} at 8 bits per sample, bias-tees were used to separate the high and low frequency components at low temperature, with the RF side connected to a microwave switch used for calibration and circulators with \SI{18}{\decibel} of isolation in order to reduce amplifier back-action noise.

During the cooldown differential resistance was measured as the gate voltage was swept from \SI{-60}{\V} to \SI{+60}{\V} in order to quantify the temperature dependence of UCF. Several different temperatures can be seen in fig.~\ref{fig:gatesweeps}.

\begin{figure}
\caption{Sample Conductance vs. Gate Voltage at several different sample temperatures. Here the growing conductance fluctuations are visible.}
\centering
\includegraphics[scale=0.6]{images/cooldown_gate_sweeps}
\label{fig:gatesweeps}
\end{figure}

In order to analyse these fluctuations a background conductance is calculated by smoothing the data and then removed to leave flat fluctuations, in all figures only the section from \SI{-60}{\V} to \SI{-30}{\V} is used in order to negate the effects of the Dirac point. Fig.~\ref{fig:cooldown_ucf} shows the standard deviation of the extracted fluctuations during cooldown.

\begin{figure}
\caption{Extracted fluctuation size vs. temperature for two different samples. Here theory values for a 1D diffusive metal wire are shown.}
\centering
\includegraphics[scale=0.6]{images/ucf_size_vs_temp}
\label{fig:cooldown_ucf}
\end{figure}

Noise calibration is performed by injecting \SI{10000}{\K} noise into two attenuated channels which are connected to the same signal path as the sample via microwave switches at the mixing chamber, with careful calibration of the attenuation in the lines the injected noise temperature can be calculated and compared against the correlation.

Sample cross-correlation is converted to a noise temperature, the noise is then scaled by the voltage over the sample -- as the background conductance changes with the gate so does the voltage over the sample -- this leads to a small increase of the noise at gate voltages closer to \SI{0}{\V}. Background subtraction for the noise fluctuations is done in the same way as conductance fluctuations.

A comparison between measured conductance fluctuations and shot-noise fluctuations can be seen in fig.~\ref{fig:noise_ucf_comparison}.

Even though our results have been obtained in a regime that is clearly off from the strongly localized regime, there may be effects beyond the weak localization as the sheet resistance is close to the quantum resistance. Consequently, the formulas based on the weak localization approach \cite{Jong1995} may be insufficient. However, our tight binding calculations indicate that strong shot noise power fluctuations remain down to quite small conductances where $F$ is already nearly 1 (see Fig. \ref{Prms}).

\begin{figure}
\caption{Plot of background-subtracted conductance fluctuations and noise fluctuations after processing. Negative conductance is plotted for clarity.}
\centering
\includegraphics[scale=0.6]{images/noise_and_ucf_compared}
\label{fig:noise_ucf_comparison}
\end{figure}

In fig.~\ref{fig:cond_dist} and fig.~\ref{fig:noise_dist} the cumulative distribution is plotted against a Gaussian distribution function\cite{horsell_mesoscopic_2009}.

%\begin{figure}
%\caption{Cumulative distribution of conductances plotted against a normal distribution.}
%\centering
%\includegraphics[scale=0.6]{images/cond_dist}
%\label{fig:cond_dist}
%\end{figure}

%
%\begin{figure}
%\caption{Cumulative distribution of noise plotted against a normal distribution.}
%\centering
%\includegraphics[scale=0.6]{images/noise_dist}
%\label{fig:noise_dist}
%\end{figure}
%
%
\begin{figure}
\caption{Standard deviation of conductance fluctuations against sample bias voltage. Theory for a metallic diffusive wire also included.}
\centering
\includegraphics[scale=0.6]{images/final_cond}
\label{fig:final_cond}
\end{figure}


\begin{figure}
\caption{Standard deviation of shot-noise fluctuations against sample bias voltage. Theory for a metallic diffusive wire also included. Lightly coloured areas show a reasonable error in noise calibration. A reasonable figure for error has been subtracted from the data.}
\centering
\includegraphics[scale=0.6]{images/final_noise}
\label{fig:final_noise}
\end{figure}


\begin{figure}
\caption{Low-bias Fano factors against gate voltage extracted from fitting with the Khlus formula. Blue line shows zero-field results and red line \SI{25}{\milli\tesla} \textbf{**(FIX add legend)**}}
\centering
\includegraphics[scale=0.6]{images/fano_factors}
\label{fig:fano_factors}
\end{figure}

%
%\begin{figure}
%\caption{Excess Fano factor against DC bias voltage, jump at \SI{1}{\micro\V} is because this is 3 data sets measured days/weeks apart and combined.}
%\centering
%\includegraphics[scale=0.6]{images/fano_vs_bias}
%\label{fig:fano_vs_bias}
%\end{figure}

%
%\begin{figure}
%\caption{Attempts at fitting weak localisation, see \cite{tikhonenko_weak_2008} for details}
%\centering
%\includegraphics[scale=0.6]{images/wl_fitting}
%\label{fig:wl_fitting}
%\end{figure}

For reasons unknown most samples had some form of non-universal resistance fluctuation fig.~\ref{fig:res_fluc}, despite investigation a change to the manufacturing process could not eliminate this problem. Resistance fluctuations were always sharp jumps, taking place over < 0.5 second timescales and always manifested at lower temperatures (< \SI{10}{\K}), typically growing with size at the lowest temperature. Different samples had different scales of this problem, with relative size (std. dev. of jumps divided by average conductance) differing as well. This particular sample had a standard deviation of 0.1 $2e^2/h$ at the gate voltages used in the measurement and this has been taken into account in plotting of data \textbf{**(FIX double check some older plots for this and check for unit errors)**}.

%\begin{figure}
%\caption{Resistance fluctuations with time}
%\centering
%\includegraphics[scale=0.6]{images/res_fluc}
%\label{fig:res_fluc}
%\end{figure}


%\begin{figure}
%\caption{FFT of resistance fluctuations}
%\centering
%\includegraphics[scale=0.6]{images/res_fluc_fft}
%\label{fig:res_fluc_fft}
%\end{figure}


\begin{figure}
\caption{Gate sweep at different static magnetic fields}
\centering
\includegraphics[scale=0.6]{images/gate_sweep_different_fields}
\label{fig:gate_sweep_different_fields}
\end{figure}

In fig.~\ref{fig:gate_sweep_different_fields} a high-resolution gate sweep was done in different static magnetic fields. In general the result is as-expected, with zero and \SI{0.1}{\milli\tesla} (corresponding to less than a flux quanta through the sample) having almost the same interference pattern, whereas at higher fields the pattern is changed.


%\begin{figure}
%\caption{Magnetoresistance at different gate voltages}
%\centering
%\includegraphics[scale=0.6]{images/magnetoresistance_all_gates}
%\label{fig:magnetoresistance_all_gates}
%\end{figure}

Unfortunately almost all magnetoresistance measurements were poor, as seen in fig.~\ref{fig:magnetoresistance_all_gates}, repeatability was poor and only minimal weak localisation was seen. A much larger range of field (to ideally above \SI{1}{\tesla}) would be needed to collect any proper statistics. Despite that a clear gate dependence can be seen.


%\begin{figure}
%\caption{High-bias Vcor vs. current at two different gate voltages}
%\centering
%\includegraphics[scale=0.6]{images/high_bias_vcor_current}
%\label{fig:high_bias_vcor_current}
%\end{figure}
%
%\begin{figure}
%\caption{High-bias Vcor vs. voltage at two different gate voltages}
%\centering
%\includegraphics[scale=0.6]{images/high_bias_vcor_voltage}
%\label{fig:high_bias_vcor_voltage}
%\end{figure}

%In fig.~\ref{fig:high_bias_vcor_current} and fig.~\ref{fig:high_bias_vcor_voltage} the raw voltage correlation and bias is plotted (this was a high-bias measurement). Fig.~\ref{fig:high_bias_fano} and fig.~\ref{fig:ep_coupling} show the resultant extracted excess Fano factor and an electron-phonon coupling plot. In particular the transition from $T^2$ to $T^4$ can be seen in the \SI{0}{\V} gate line. For reasons of sample safety and the fact that the DC-lines and sample were quite resistive the bias was not pushed any higher than this.
%
%\begin{figure}
%\caption{Excess Fano factor vs. current high-bias measurement at two different gate voltages.}
%\centering
%\includegraphics[scale=0.6]{images/high_bias_fano}
%\label{fig:high_bias_fano}
%\end{figure}
%
%
%\begin{figure}
%\caption{High-bias electron-phonon coupling}
%\centering
%\includegraphics[scale=0.6]{images/ep_coupling}
%\label{fig:ep_coupling}
%\end{figure}



Our difference between conductance and shot noise fluctuations can be understood as variation between diffusive wire and chaotic quantum dot type of behavior. More exactly, the symmetry of the bimodal transmission eigenvalue distribution has to be nearly preserved while variation of transmission channels takes place.  The noise variation arises from channels with transmission $T \sim 0$ and 1 changing approximately symmetrically to channels with $T\simeq0.5$. Such a change does not alter much the conductance but induces the largest possible change in the shot noise proportional to $T(1-T)$. Hence,  distinctly different values for the Fano factor can be produced during the gate sweep.

Dephasing length becomes $\ell_{\varphi} = 1.3$ $\mu$m at sheet resistance 1 k$\Omega$. This corresponds to a mean free path of xxx nm which yields $L_T < \ell_{\varphi}$. 

As $L_T$ and $ \ell_{\varphi}$ are nearly equal we employ only the leading order decay of fluctuations with dephasing which reads $ \delta G \propto ()\ell_{\varphi}/L)^2$ \cite{akkermans}. Adopting the 1d wire result of Ref. \onlinecite{de_jong_mesoscopic_1992} for shot noise ower fluctuations we expect to have  $ \delta G \propto ()\ell_{\varphi}/L)^3$, i.e. the decay exponent is increased by 1.

When electron-electron interactions are strong, the effective electronic temperature is proportional to the bias voltage $V$ by relation $T_e=\frac{F}{2}\frac{eV}{k_B}$ \cite{Wan2010}. Consequently, The simplest approximation for the decay of fluctuations as a function of bias is obtained by inserting $T_e$ to the decay formulas $ \delta G(\ell_{\varphi})$ and $ S_P(\ell_{\varphi})$. This yields voltage dependence of $1/\sqrt{V}$ $ \delta G$ and $S_P \propto V^{1/4}$ in 2d and $1/V^{3/4}$ and $ V^{-1/4}$, respectively, in quasi 1d. The latter values are seen to agree with the experimental results in Figs. \ref{fig:final_cond} and \ref{fig:final_noise}.

In simple terms, the electron-electron interaction effects become important in graphene because the sheet resistance of graphene is substantial. This leads to large noise and thereby to voltage fluctuations that cause relaxation on a length scale $\sim L$.
Large electron electron scattering in transport experiments was observed e.g in Ref. \onlinecite{voutilainen2010} in which an enhancement factor of 100 in relation to regular disordered metals was found.

Simulations

Basic findings from the experiment


\bibliographystyle{unsrt}
\bibliography{refs}


\end{document}
\grid
